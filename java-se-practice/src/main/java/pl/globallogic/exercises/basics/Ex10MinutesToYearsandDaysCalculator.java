package pl.globallogic.exercises.basics;

public class Ex10MinutesToYearsandDaysCalculator {
    public static void printYearsAndDays(long minutes){
        long years,minToDays,hours,days;
        if (minutes<0){
            System.out.println("Invalid Value");
        }
        else{
            hours=minutes/60;
            minToDays=hours/24;
            years=minToDays/365;//ilość otrzymanych dni dzielona przez  365 dni w roku=ilość lat
            days=minToDays%365;//reszta od tego to dni;

            System.out.println(String.format("%s min= %s y and %s d",minutes,years,days));
        }

    }
    public static void main(String[] args) {
        printYearsAndDays(561600);
    }
}
