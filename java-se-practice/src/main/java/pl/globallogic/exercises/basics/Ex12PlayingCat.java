package pl.globallogic.exercises.basics;

public class Ex12PlayingCat {
    public static boolean isCatPlaying(boolean summer,int temperature){
        if ((25<temperature&&temperature<=35)&&summer==false){
            return true;
        }
        else if ((25<temperature&&temperature<=45)&&summer==true){
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        System.out.println(isCatPlaying(false,35));
    }
}
