package pl.globallogic.exercises.basics;
import java.util.Scanner;
public class Ex1PositiveNegativeZero {
    public static void checkNumber(int number){
        if(number>0){
            System.out.println("positive");
        }
        else if(number<0){
            System.out.println("negative");
        }
        else{
            System.out.println("zero");
        }    }
    public static void main(String[] args) {
        boolean T=true;
        Scanner scanner = new Scanner(System.in);
        while(T){
            System.out.println("Enter a number");
            int number=scanner.nextInt();
            checkNumber(number);
        }


    }
}
