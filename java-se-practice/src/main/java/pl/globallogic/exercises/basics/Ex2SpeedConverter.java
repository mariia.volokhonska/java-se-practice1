package pl.globallogic.exercises.basics;
import java.util.Scanner;
public class Ex2SpeedConverter {
    public static long toMilesPerHour(double kilometersPerHour){
        double milesPerHour=0.0;
        if(kilometersPerHour>=0){
            milesPerHour=kilometersPerHour/1.6093440006147;
            long milesPerHourLong=Math.round(milesPerHour);
            return milesPerHourLong;
        }
        return -1;
    }
    public static void printConversation(double kilometersPerHour){
        double milesPerHour;
        if(kilometersPerHour<0) {
            System.out.println("Invalid Value");
        }
        else if(kilometersPerHour>=0){
            milesPerHour=kilometersPerHour/1.6093440006147;
            long milesPerHourLong=Math.round(milesPerHour);
            System.out.println(kilometersPerHour+" km/h = "+milesPerHourLong+" mi/h");
        }
    }

    public static void main(String[] args) {
        System.out.println(toMilesPerHour(0));
        printConversation(10.25);
    }
}
