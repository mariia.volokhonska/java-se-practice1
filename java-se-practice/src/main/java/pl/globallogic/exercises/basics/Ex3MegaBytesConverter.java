package pl.globallogic.exercises.basics;

public class Ex3MegaBytesConverter {
    public static void printMegaBytesAndKiloBytes(int kiloBytes){
        int megaBytes;
        int kiloBytesReszta;
        if(kiloBytes<0){
            System.out.println("Invalid Value");
        }
        else{
            megaBytes=kiloBytes/1024;
            kiloBytesReszta=kiloBytes%1024;
            System.out.println(
                String.format("%s KB = %s MB and %s KB"
            ,kiloBytes,megaBytes,kiloBytesReszta));
        }
    }

    public static void main(String[] args){
        printMegaBytesAndKiloBytes(2500);
    }
}
