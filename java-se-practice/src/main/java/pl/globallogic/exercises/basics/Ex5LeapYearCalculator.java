package pl.globallogic.exercises.basics;

public class Ex5LeapYearCalculator {
    public static boolean isLeapYear(int year){
        if (year>=1&&year<=9999){
            if(year%4==0){
                if(year%100==0){
                    if(year%400==0){
                        return true;
                    }
                }
            }

        }
        return false;
    }
    public static void main(String[] args) {
        System.out.println(isLeapYear(2000));
    }
}
