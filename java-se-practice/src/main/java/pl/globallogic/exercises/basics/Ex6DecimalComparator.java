package pl.globallogic.exercises.basics;

public class Ex6DecimalComparator {
    public static boolean areEqualByThreeDecimalPlaces(double a,double b){
        double c=Math.abs(a);
        double d=Math.abs(b);
        c=Math.floor(c*1000);
        d=Math.floor(d*1000);
        if(a<0&&b<0){
            c*=-1;
            d*=-1;
        }
        if(a>0&&b<0){
            d*=-1;
        }
        if(a<0&&b>0){
            c*=-1;
        }
        if(c==d){
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        System.out.println(areEqualByThreeDecimalPlaces(3.0,3.0));
    }
}
