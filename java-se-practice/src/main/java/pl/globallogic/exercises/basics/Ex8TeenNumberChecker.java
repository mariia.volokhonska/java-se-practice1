package pl.globallogic.exercises.basics;

public class Ex8TeenNumberChecker {
    public static boolean hasTeen(int a, int b , int c){
        if((13<=a && a<=19)||(13<=b && b<=19)||(13<=c && c<=19)){
            return true;
        }
        return false;
    }
    public static boolean isTeen(int k){
        if(13<=k&&k<=19){
            return true;
        }
        else{
            return false;
        }
    }
    public static void main(String[] args) {
        System.out.println(hasTeen(22,23,24));
        System.out.println(isTeen(13));
    }
}
