package pl.globallogic.exercises.basics;

public class Ex9AreaCalculator {
    public static double area(double radius){
        if(radius<0){
            return -1.0;
        }
        return radius*radius*Math.PI;
    }
    public static double area(double x,double y){
        if(x<0||y<0){
            return -1.0;
        }
        double areaRectangle=x*y;
        return areaRectangle;
    }
    public static void main(String[] args) {
        System.out.println("area of circle="+area(5.0)+" area of rectangle="+area(5.0,4.0));
    }
}
