package pl.globallogic.exercises.intermediate;

public class Ex16NumberPalindrome {
    public static boolean isPalindrome(int number){
        if(number<0){
            number*=-1;
        }
        String numberInString=String.valueOf(number);
        StringBuilder str=new StringBuilder(numberInString);
        str.reverse();
        String numberInStringReveresed=str.toString();
        if (numberInString.equals(numberInStringReveresed)){
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        System.out.println(isPalindrome(-1221));
    }
}
