package pl.globallogic.exercises.intermediate;

public class Ex17firstAndLastDigitSum {
    public static int sumFirstAndLastDigit(int number){
        int first,last,sum;
        first=0;
        if(number<0){
            return -1;
        }

        String numberInString=Integer.toString(number);
        last=number%10;
        for(int i=0;i<numberInString.length();i++){
            first=number%10;
            number/=10;
        }
        sum=first+last;
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(sumFirstAndLastDigit(257));
    }
}
