package pl.globallogic.exercises.intermediate;

public class Ex18EvenDigitSum {
    public static int getEvenDigitSum(int number){
        int sum=0;
        if(number<0){
            return -1;
        }
        String numberInString=String.valueOf(number);
        char[] numberCharArray=numberInString.toCharArray();
        for(int i=0;i<numberCharArray.length;i++){
            int currentNumber=Character.getNumericValue(numberCharArray[i]);
            if(currentNumber%2==0){
                sum+=currentNumber;
            }
        }
        return sum;

    }

    public static void main(String[] args) {
        System.out.println(String.format("The sum of even numbers are: %d",getEvenDigitSum(123456789)));
    }
}
