package pl.globallogic.exercises.intermediate;

public class Ex19SharedDigit {
    public static boolean hasSharedDigit(int a,int b){
        if((a<10||a>99)||(b<10||b>99)){
            return false;
        }
        String aToString=String.valueOf(a);
        String bToString=String.valueOf(b);
        char[] charArray=aToString.toCharArray();
        char[] charMatches;
        charMatches=new char[20];
        int j=0;
        for(int i=0;i<charArray.length;i++){
            if(bToString.contains(Character.toString(charArray[i]))){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println((hasSharedDigit(15,55)));
    }
}
