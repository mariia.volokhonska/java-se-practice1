package pl.globallogic.exercises.intermediate;

public class Ex20LastDigitChecker {
    public static boolean hasSameLastDigit(int a,int b,int c){
        if((a<10||a>99)||(b<10||b>99)||(c<10||c>99)){
            return false;
        }
        String aToString=Integer.toString(a);
        String bToString=Integer.toString(b);
        String cToString=Integer.toString(c);
        char[] aCharArray=aToString.toCharArray();
        char[] bCharArray=bToString.toCharArray();
        char[] cCharArray=cToString.toCharArray();
        char lastA=aCharArray[aCharArray.length-1];
        char lastB=bCharArray[bCharArray.length-1];
        char lastC=cCharArray[cCharArray.length-1];
        if(lastA==lastB||lastB==lastC||lastA==lastC){
            return true;
        }
        return false;



    }
    public static void main(String[] args) {
        System.out.println(hasSameLastDigit(41,22,71));
    }
}
