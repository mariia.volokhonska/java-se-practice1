package pl.globallogic.exercises.intermediate;

public class Ex21AllFactors {
    public static void printFactors(int number){
        if(number<0){
            System.out.println("Invalid value");
        }
        String string="";
        int i=1;
        while(i<=number){
            if(number%i==0){
                string+=i+" ";
            }
            i++;
        }

        System.out.println(string);
    }
    public static void main(String[] args) {
        printFactors(32);
    }
}
