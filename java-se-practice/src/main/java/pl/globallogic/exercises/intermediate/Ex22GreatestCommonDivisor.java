package pl.globallogic.exercises.intermediate;

public class Ex22GreatestCommonDivisor {
    public static int getGreatestCommonDivisor(int first, int second) {
        int mid;

        if (first < second) {
            mid = first;
            first = second;
            second = mid;
        }
        if (second <= 0) {
            return first;
        }
        return getGreatestCommonDivisor(first - second, second);
    }
    public static boolean checkValue(int first,int last){
        if(first<10||last<10){
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        int first=24;
        int last=32;
        if(checkValue(first,last)){
            System.out.println("The greatest common divisor is: "+getGreatestCommonDivisor(first, last));
        }
        else{
            System.out.println("Invalid value");
        }

    }
}
