package pl.globallogic.exercises.intermediate;

public class Ex23PerfectNumber {
    public static boolean isPerfectNumber(int number){
        if(number<1){
            return false;
        }
        int[] intArray;
        intArray=new int[20];
        int i=1;
        int sum=0;
        while(i<number){
            if(number%i==0){
                sum+=i;
            }
            i++;
        }
        if (sum==number){
            return true;
        }

        return false;

    }
    public static void main(String[] args) {
        int number=28;
        System.out.println(String.format("Number %d is perfect number? Answer: %b",number,isPerfectNumber(number)));
    }
}
