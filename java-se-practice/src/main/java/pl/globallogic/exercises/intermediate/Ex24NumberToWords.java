package pl.globallogic.exercises.intermediate;

public class Ex24NumberToWords {
    public static void numberToWords(int number){
        int lastDigit;
        int reversedNumber;
        if(number<0){
            System.out.println("Invalid Value");
        }
        reversedNumber=reverse(number);
        if(getDigitCount(number)!=getDigitCount(reversedNumber)){
            System.out.print("ONE ");
            for(int i=1;i<getDigitCount(number);i++){
                System.out.print("ZERO ");
            }
        }
        else{
            while(reversedNumber>0){
                lastDigit=reversedNumber%10;
                reversedNumber/=10;
                switch(lastDigit){
                    case 0:
                        System.out.print("ZERO ");break;
                    case 1:
                        System.out.print("ONE ");break;
                    case 2:
                        System.out.print("TWO ");break;
                    case 3:
                        System.out.print("THREE ");break;
                    case 4:
                        System.out.print("FOUR ");break;
                    case 5:
                        System.out.print("FIVE ");break;
                    case 6:
                        System.out.print("SIX ");break;
                    case 7:
                        System.out.print("SEVEN ");break;
                    case 8:
                        System.out.print("EIGHT ");break;
                    case 9:
                        System.out.print("NINE ");break;
                    default:
                        System.out.print("Invalid Value");
                }
            }
        }


    }
    public static int reverse(int number){
        char mid;
        String numberString=String.valueOf(number);
        char[] charArray=numberString.toCharArray();
        int i=0;
        int j=charArray.length-1;
        while(j>i){
           mid=charArray[i];
           charArray[i]=charArray[j];
           charArray[j]=mid;
           i++;
           j--;
        }
        numberString=String.valueOf(charArray);
        int reversedNumber=Integer.valueOf(numberString);
        return reversedNumber;



    }
    public static int getDigitCount(int number){
        int counter=0;
        if (number<0){
            return -1;
        }
        while(number>0){
            number/=10;
            counter++;
        }
        return counter;
    }

    public static void main(String[] args) {
        numberToWords(100);
    }
}
