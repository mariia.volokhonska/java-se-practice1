package pl.globallogic.exercises.intermediate;

public class Ex25FlourPackProblem {
    public static boolean canPack(int bigCount,int smallCount,int goal){
        int template=0;

        if(bigCount<0||smallCount<0||goal<0){
            return false;
        }
        while(bigCount*5>goal){
            bigCount--;
        }

        if(bigCount>0){
            template+=bigCount*5;
        }

        while((template<goal)&&(smallCount>0)){
            template+=1;
            smallCount--;

        }
        if(template==goal){
            return true;
        }

        return false;
    }
    public static void main(String[] args) {
        System.out.println(canPack(2,2,11));
    }
}
