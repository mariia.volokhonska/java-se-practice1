package pl.globallogic.exercises.intermediate;

import java.util.ArrayList;
import java.util.Collections;

public class Ex26LargestPrime {
    public static ArrayList<Integer> allFactors(int number){
        ArrayList<Integer> listFactors=new ArrayList<Integer>();
        for(int i=1;i<=Math.sqrt(number);i++){
            if(number%i==0){
                listFactors.add(i);
                if(number/i!=i){
                    listFactors.add(number/i);
                }
            }

        }
        System.out.println(listFactors);

        return listFactors;
    }
    public static boolean isPrime(int num){
        for(int i=2;i<=Math.sqrt(num);i++){
            if(num%i==0){
                return false;
            }
        }
        return true;
    }
    

    public static int getLargestPrime(int number){
        if(number<0){
            return -1;
        }
        ArrayList<Integer> listFactors=new ArrayList<Integer>();
        ArrayList<Integer> listPrimes=new ArrayList<Integer>();
        listFactors=allFactors(number);
        for (int factor:
             listFactors) {
            if(isPrime(factor)){
                listPrimes.add(factor);
            }
        }
        if(listPrimes.isEmpty()){
            return -1;
        }
        else{
            Collections.sort(listPrimes);
            System.out.println(listPrimes);
            return listPrimes.get(listPrimes.size()-1);
        }

    }

    public static void main(String[] args) {
        int number=56;
        System.out.println("The largest prime factor of this number is: "+getLargestPrime(number));
    }
}
