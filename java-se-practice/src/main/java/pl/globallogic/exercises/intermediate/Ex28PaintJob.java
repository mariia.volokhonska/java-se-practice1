package pl.globallogic.exercises.intermediate;

public class Ex28PaintJob {
    public static int getBucketCount(double width,double height,double areaPerBucket,int extraBuckets){
        if(width<=0||height<=0||areaPerBucket<=0||extraBuckets<0){
            return -1;
        }
        double area;
        int counter=0;
        area=width*height;
        double painterCanCoverNow=extraBuckets*areaPerBucket;
        if(area<=painterCanCoverNow){
            return 0;
        }
        else{
            while(area>painterCanCoverNow){
                painterCanCoverNow+=areaPerBucket;
                counter+=1;
            }
            return counter;
        }

    }
    public static int getBucketCount(double width,double height,double areaPerBucket){
        if(width<=0||height<=0||areaPerBucket<=0){
            return -1;
        }
        double area;
        int counter=0;
        area=width*height;
        double painterCanCover=0;
        while(area>painterCanCover){
            painterCanCover+=areaPerBucket;
            counter+=1;
        }
        return counter;
    }
    public static int getBucketCount(double area, double areaPerBucket){
        if(area<=0||areaPerBucket<=0){
            return -1;
        }
        double coverage=0;
        int counter=0;
        while(area>coverage){
            coverage+=areaPerBucket;
            counter+=1;
        }
        return counter;
    }


    public static void main(String[] args) {
        System.out.println("1.The amount of buckets painter needs to buy: "+getBucketCount(2.75,3.25,2.5,1));
        System.out.println("2.The amount of buckets painter needs to buy: "+getBucketCount(7.25,4.3,2.35));
        System.out.println("3.The amount of buckets painter needs to buy: "+getBucketCount(3.26,0.75));

    }
}
