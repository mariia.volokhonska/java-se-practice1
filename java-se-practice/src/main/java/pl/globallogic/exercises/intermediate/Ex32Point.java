package pl.globallogic.exercises.intermediate;

public class Ex32Point {
    private int x;
    private int y;
    public Ex32Point(){

    }
    public Ex32Point(int x,int y){
        this.x=x;
        this.y=y;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public void setX(int x){
        this.x=x;
    }
    public void setY(int y){
        this.y=y;
    }
    public double distance(){
        return Math.sqrt((x*x+y*y));
    }
    public double distance(int x,int y){
        double dist=Math.sqrt((x-this.x)*(x-this.x)+(y-this.y)*(y-this.y));
        return dist;
    }
    public double distance(Ex32Point point){
        double dist=Math.sqrt((point.x-this.x)*(point.x-this.x)+(point.y-this.y)*(point.y-this.y));
        return dist;
    }

    public static void main(String[] args) {
        Ex32Point first = new Ex32Point(6, 5);
        Ex32Point second = new Ex32Point(3, 1);
        System.out.println("distance(0,0)= " + first.distance());
        System.out.println("distance(second)= " + first.distance(second));
        System.out.println("distance(2,2)= " + first.distance(2, 2));
        Ex32Point point = new Ex32Point();
        System.out.println("distance()= " + point.distance());
    }
}
