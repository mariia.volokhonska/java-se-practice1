package pl.globallogic.exercises.intermediate;

public class Ex34ComplexOperations {
    private double real;
    private double imaginary;
    public Ex34ComplexOperations(double real,double imaginary){
        this.real=real;
        this.imaginary=imaginary;
    }
    public double getReal(){
        return real;
    }

    public double getImaginary() {
        return imaginary;
    }
    public void add(double real,double imaginary){
        this.real=this.real+real;
        this.imaginary=this.imaginary+imaginary;
    }
    public void add(Ex34ComplexOperations number){
        this.real=number.real+this.real;
        this.imaginary=this.imaginary+number.imaginary;
    }
    public void subtract(double real,double imaginary){
        this.real=this.real-real;
        this.imaginary=this.imaginary-imaginary;
    }
    public void subtract(Ex34ComplexOperations number){
        this.real=this.real-number.real;
        this.imaginary=this.imaginary-number.imaginary;
    }

    public static void main(String[] args) {
        Ex34ComplexOperations one=new Ex34ComplexOperations(1.0,1.0);
        Ex34ComplexOperations number=new Ex34ComplexOperations(2.5,-1.5);
        one.add(1,1);
        System.out.println("one.real= "+one.getReal());
        System.out.println("one.imaginary= " + one.getImaginary());
        one.subtract(number);
        System.out.println("one.real= " + one.getReal());
        System.out.println("one.imaginary= " + one.getImaginary());
        number.subtract(one);
        System.out.println("number.real= " + number.getReal());
        System.out.println("number.imaginary= " + number.getImaginary());

    }
}
