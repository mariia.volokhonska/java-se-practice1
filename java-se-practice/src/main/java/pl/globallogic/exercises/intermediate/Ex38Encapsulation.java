package pl.globallogic.exercises.intermediate;

public class Ex38Encapsulation {
    private int tonerLevel;
    private int pagesPrinted;
    private boolean duplex;
    public Ex38Encapsulation(int tonerLevel,boolean duplex){
        if(tonerLevel>-1&&tonerLevel<=100){
            this.tonerLevel=tonerLevel;
        }
        else{
            this.tonerLevel=-1;
        }
        this.duplex=duplex;
    }

    public void setPagesPrinted() {
        this.pagesPrinted = 0;
    }
    public int addToner(int tonerAmount){
        if(tonerAmount>0&&tonerAmount<=100){
            if(tonerAmount+tonerLevel<=100){
                tonerLevel+=tonerAmount;
                return tonerLevel;
            }
        }
        return -1;
    }
    public int printPages(int pages){
        int pagesToPrint=pages;
        if(duplex){
            pagesToPrint/=2;
            pagesPrinted+=pagesToPrint;

        }
        return pagesToPrint;
    }

    public int getPagesPrinted() {
        return pagesPrinted;
    }

    public static void main(String[] args) {
        Ex38Encapsulation printer = new Ex38Encapsulation(50, true);
        System.out.println(printer.addToner(50));
        System.out.println("initial page count = " +printer.getPagesPrinted());
        int pagesPrinted = printer.printPages(4);
        System.out.println("Pages printed was " + pagesPrinted +" new total print count for printer = " +printer.getPagesPrinted());
        pagesPrinted = printer.printPages(2);
        System.out.println("Pages printed was " + pagesPrinted +" new total print count for printer = " +printer.getPagesPrinted());
    }
}
