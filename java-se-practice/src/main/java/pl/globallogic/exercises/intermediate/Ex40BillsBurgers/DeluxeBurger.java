package pl.globallogic.exercises.intermediate.Ex40BillsBurgers;

public class DeluxeBurger extends Hamburger{


    public DeluxeBurger(){
        super( 19.10, "DeluxeBurger","","");
        System.out.println("Added Drink for an extra 1.5");
        System.out.println("Added Chips for an extra 2.75");

    }
    @Override public void addHamburgerAddition1(String addition1Name,double addition1Price){
        addition1Name="Drink";
        addition1Price=1.5;
        System.out.println("Cannot not add additional items to a deluxe burger\n");
    }
    @Override public void addHamburgerAddition2(String addition2Name,double addition2Price){
        addition2Name="Chips";
        addition2Price=2.75;
        System.out.println("Cannot not add additional items to a deluxe burger\n");
    }
    @Override public void addHamburgerAddition3(String addition3Name,double addition3Price){
        System.out.println("Cannot not add additional items to a deluxe burger\n");
    }
    @Override public void addHamburgerAddition4(String addition4Name,double addition4Price){
        System.out.println("Cannot not add additional items to a deluxe burger\n");
    }
}
