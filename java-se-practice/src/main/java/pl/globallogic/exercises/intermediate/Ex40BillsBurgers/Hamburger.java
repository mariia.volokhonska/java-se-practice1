package pl.globallogic.exercises.intermediate.Ex40BillsBurgers;

public class Hamburger {
    protected double price;
    protected String name;
    protected String meat;
    protected String breadRollType;
    private String addition1Name="";
    private double addition1Price=0;
    private String addition2Name="";
    private double addition2Price=0;
    private String addition3Name="";
    private double addition3Price=0;
    private String addition4Name="";
    private double addition4Price=0;

    public Hamburger(double price, String name, String meat, String breadRollType) {
        this.price = price;
        this.name = name;
        this.meat = meat;
        this.breadRollType = breadRollType;
    }
    public void addHamburgerAddition1(String addition1Name,double addition1Price){
        this.addition1Name=addition1Name;
        this.addition1Price=addition1Price;
        System.out.printf("Added %s for an extra %s\n",addition1Name,addition1Price);
    }
    public void addHamburgerAddition2(String addition2Name,double addition2Price){
        this.addition2Name=addition2Name;
        this.addition2Price=addition2Price;
        System.out.printf("Added %s for an extra %s \n",addition2Name,addition2Price);
    }
    public void addHamburgerAddition3(String addition3Name,double addition3Price){
        this.addition3Name=addition3Name;
        this.addition3Price=addition3Price;
        System.out.printf("Added %s for an extra %s\n",addition3Name,addition3Price);
    }
    public void addHamburgerAddition4(String addition4Name,double addition4Price){
        this.addition4Name=addition4Name;
        this.addition4Price=addition4Price;
        System.out.printf("Added %s for an extra %s\n",addition4Name,addition4Price);
    }
    public double itemizeHamburger(){
        double total=price;
        if((!addition1Name.isEmpty())&&(addition1Price>0)){

            total+=addition1Price;
        }
        if((!addition2Name.isEmpty())&&(addition2Price>0)){

            total+=addition2Price;
        }
        if((!addition3Name.isEmpty())&&(addition3Price>0)){

            total+=addition3Price;
        }
        if((!addition4Name.isEmpty())&&(addition4Price>0)){

            total+=addition4Price;
        }
        return total;
    }

    public static void main(String[] args) {
        Hamburger hamburger = new Hamburger(3.56, "Sausage", "Basic", "White");
        hamburger.addHamburgerAddition1("Tomato", 0.27);
        hamburger.addHamburgerAddition2("Lettuce", 0.75);
        hamburger.addHamburgerAddition3("Cheese", 1.13);
        System.out.println("Total Burger price is " + hamburger.itemizeHamburger());

        HealthyBurger healthyBurger = new HealthyBurger(5.67, "Bacon");
        healthyBurger.addHealthyAddition1("Egg", 5.43);
        healthyBurger.addHealthyAddition2("Lentils", 3.41);
        System.out.println("Total Healthy Burger price is  " + healthyBurger.itemizeHamburger());

        DeluxeBurger db = new DeluxeBurger();
        db.addHamburgerAddition3("Should not do this", 50.53);
        System.out.println("Total Deluxe Burger price is " + db.itemizeHamburger());
    }
}
