package pl.globallogic.exercises.intermediate.Ex40BillsBurgers;

public class HealthyBurger extends Hamburger {
   private String healthyExtra1Name="";
   private double healthyExtra1Price=0;
   private String healthyExtra2Name="";
   private double healthyExtra2Price=0;
   private String meat;
   public HealthyBurger(double price,String meat){
       super(price,"HealthyBurger",meat,"");

   }
   public void addHealthyAddition1(String healthyExtra1Name,double healthyExtra1Price){
       this.healthyExtra1Name=healthyExtra1Name;
       this.healthyExtra1Price=healthyExtra1Price;
       System.out.printf("Added %s for an extra %s\n",healthyExtra1Name,healthyExtra1Price);
   }
    public void addHealthyAddition2(String healthyExtra2Name,double healthyExtra2Price){
       this.healthyExtra2Name=healthyExtra2Name;
       this.healthyExtra2Price=healthyExtra2Price;
        System.out.printf("Added %s for an extra %s\n",healthyExtra2Name,healthyExtra2Price);
    }

    @Override public double itemizeHamburger() {
       double total=price;
        if((!healthyExtra1Name.isEmpty())&&(healthyExtra1Price>0)){
            total+=healthyExtra1Price;
        }
        if((!healthyExtra2Name.isEmpty())&&(healthyExtra2Price>0)){
            total+=healthyExtra2Price;
        }
        return total;
    }
}

