package pl.globallogic.exercises.intermediate;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;

public class Ex41SortedArray {
    public static int[] getIntegers(int amount){
        Scanner scanner=new Scanner(System.in);
        int[] array={2,3,4,5,6};
        for(int i=0;i<=amount-1;i++) {
            array[i] = scanner.nextInt();

        }

        return array;
    }
    public static void printArray(int[] array){
        for(int i=0;i<array.length;i++){
            System.out.printf("Element %s contents %s \n",i,array[i]);
        }
    }
    public static int[] sortIntegers(int[] array){
        int mid;
        for(int i= array.length-1;i>0;i--){
            for(int j=i;j>0;j--){
                if(array[j]>array[j-1]){
                    mid=array[j];
                    array[j]=array[j-1];
                    array[j-1]=mid;

                }

            }
        }
        return array;
    }
    public static void main(String[] args) {
        int amount=5;
        int[] arrayToSort=new int[amount];
        int[] sortedArray=new int[amount];
        arrayToSort=getIntegers(amount);
        printArray(arrayToSort);
        sortedArray=sortIntegers(arrayToSort);
        System.out.println(Arrays.toString(sortedArray));



    }


}
