package pl.globallogic.exercises.intermediate;
import java.util.Scanner;

public class Ex42MinimumElement {

    public static int readInteger(){
        int amountNumber;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter amount of numbers in array\n");
        amountNumber= scanner.nextInt();

        return amountNumber;
    }
    public static int[] readElements(int amount){

        int[] array=new int[amount];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter elements of array");
        for(int i=0;i<amount;i++){
            array[i]=scanner.nextInt();
        }
        return array;
    }
    public static int findMin(int[] array){
        int min=array[0];
        for(int i=1;i<array.length;i++){
            if(array[i]<min){
                min=array[i];
            }
        }
        return min;
    }
    public static void main(String[] args) {
        System.out.printf("Minimum element in array is: %s",findMin(readElements(readInteger())));

    }
}
