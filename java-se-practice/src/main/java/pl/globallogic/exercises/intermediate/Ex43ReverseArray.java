package pl.globallogic.exercises.intermediate;

import java.util.Arrays;

public class Ex43ReverseArray {
    public static void reverse(int[] array){
        int mid;
        System.out.println("Array is: "+Arrays.toString(array));
        for(int i=0;i<=(array.length-1)/2;i++){
            mid=array[i];
            array[i]=array[array.length-i-1];
            array[array.length-i-1]=mid;

        }
        System.out.println("Reversed array is: "+Arrays.toString(array));
    }
    public static void main(String[] args) {
        int[]array={1,2,3,4,5};
        reverse(array);
    }
}
