package pl.globallogic.exercises.intermediate.Ex44MobilePhone;

import java.util.ArrayList;
import java.util.Collection;

public class MobilePhone {
    private String myNumber;
    private ArrayList<Contact> myContacts=new ArrayList<Contact>();
    public MobilePhone(String myNumber,ArrayList<Contact> myContacts){
        this.myNumber=myNumber;
        this.myContacts=myContacts;
    }
    public boolean addNewContact(Contact newContact){
        if(myContacts.contains(newContact)){
            return false;
        }
        return true;
    }
    public boolean updateContact(Contact oldContact,Contact updatedContact){
        if(myContacts.contains(oldContact)){
            int index=myContacts.indexOf(oldContact);
            myContacts.set(index,updatedContact);
            return true;
        }
        else{
            return false;
        }
    }
    public boolean removeContact(Contact contactToRemove){
        if(myContacts.contains(contactToRemove)){
            myContacts.remove(contactToRemove);
            return true;
        }
        else{
            return false;
        }
    }
    public int findContact(Contact contactToFind){
        if(myContacts.contains(contactToFind)){
            return myContacts.indexOf(contactToFind);
        }
        else{
            return -1;
        }
    }
    public int findContact(String numberToFind){
        for (Contact contact:
             myContacts) {
            if(numberToFind.equals(contact.getPhoneNumber())){
                return myContacts.indexOf(contact);
            }
        }
        return -1;
    }
    public Contact queryContact(String nameOfContact){
        for (Contact contact:
                myContacts) {
            if(nameOfContact.equals(contact.getName())){
                return contact;
            }
        }
        return null;
    }
    public void printContatcts(){
        int i=1;
        for (Contact contact:
            myContacts ) {
            System.out.printf("%s. %s -> %s \n",i,contact.getName(),contact.getPhoneNumber());
            i++;
        }
    }



}
