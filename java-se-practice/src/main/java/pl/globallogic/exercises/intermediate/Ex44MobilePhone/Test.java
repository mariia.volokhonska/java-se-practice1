package pl.globallogic.exercises.intermediate.Ex44MobilePhone;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        ArrayList<Contact> contactList=new ArrayList<>();
        Contact contact1=new Contact("Bob","31415926");
        contactList.add(contact1);
        Contact contact2=Contact.createContact("Alice","16180339");
        contactList.add(contact2);
        Contact contact3=Contact.createContact("Cyrilla","111111111111111");
        contactList.add(contact3);
        Contact contact4=Contact.createContact("Napoleon","1234556");
        contactList.add(contact4);
        MobilePhone myPhone=new MobilePhone("33322292929",contactList);
        if(myPhone.addNewContact(contact4)){
            contactList.add(contact4);
        }
        Contact contact2_1=new Contact("Geralt","000101001");
        myPhone.updateContact(contact2,contact2_1);
        myPhone.removeContact(contact1);
        System.out.printf("The contact you search for is located on the position %s in list of contacts(by object)\n",myPhone.findContact(contact4)+1);
        System.out.printf("The contact you search for is located on the position %s in list of contacts(by number)\n",myPhone.findContact("111111111111111")+1);
        System.out.printf("Information about %s 's contact: \n",myPhone.queryContact("Napoleon"));
        System.out.println("Current list of contacts:");
        myPhone.printContatcts();

    }
}
