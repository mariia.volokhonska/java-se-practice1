package pl.globallogic.exercises.intermediate.Ex45Playlist;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;


    private ArrayList<Song> songs;
    public void setSongs(ArrayList<Song> songs) {
        this.songs=songs;
    }

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
    }

    public boolean addSong(String titleOfSong,double durationOfSong){
        Song newSong=new Song(titleOfSong,durationOfSong);
        if(songs.contains(newSong)){
                return false;
        }
        songs.add(newSong);
        return true;


    }
    public Song findSong(String titleOfSong){
        for (Song song:
           songs  ) {
            if(titleOfSong.equals(song.getTitle())){
                return song;
            }
        }
        return null;
    }
    public boolean addToPlayList(int trackNumberInAlbum, LinkedList<Song> playlist){
        if(trackNumberInAlbum<=songs.size()){
            if(!playlist.contains(songs.get(trackNumberInAlbum-1))){
                playlist.addFirst(songs.get(trackNumberInAlbum-1));
                return true;}
        }

        return false;

    }
    public boolean addToPlayList(String titleOfSong, LinkedList<Song> playlist){
        for (Song song:
            songs ) {
            if(titleOfSong.equals(song.getTitle())) {
                if(!playlist.contains(song)) {
                    playlist.addFirst(song);
                    return true;
                }

            }

        }
        return false;
    }
    public static void showPlayList(LinkedList<Song> playList){
        int i=1;
        for (Song song:
            playList ) {
            System.out.printf("%s.Song and Duration: %s\n",i,song.toString());
            i++;
        }
    }
}
