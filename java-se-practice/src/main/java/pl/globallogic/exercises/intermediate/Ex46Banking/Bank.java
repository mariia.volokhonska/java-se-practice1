package pl.globallogic.exercises.intermediate.Ex46Banking;

import java.util.ArrayList;

public class Bank {
    private String name;
    private ArrayList<Branch> branches;
    public Bank(String name){
        this.name=name;
        branches=new ArrayList<>();

    }
    public boolean addBranch(String nameOfBranch){
        Branch branch=new Branch(nameOfBranch);
        if(branches.contains(branch)){
            return false;
        }
        branches.add(branch);
        return true;
    }
    public boolean addCustomer(String branch,String nameOfCustomer, double initTransaction){
        for (Branch branch1:
            branches ) {
            if((branch1.getName()).equals(branch)){
                branch1.newCustomer(nameOfCustomer,initTransaction);
                return true;
            }
        }
        return false;
    }
    public boolean addCustomerTransaction(String nameOfBranch,String nameOfCustomer,double transaction){
        boolean flag=false;
        for (Branch branch2:
            branches) {
            if(branch2.getName().equals(nameOfBranch)){
                branch2.addCustomerTransaction(nameOfCustomer,transaction);
                flag=branch2.getCustomers().stream().filter(customer -> customer.getTransactions().contains(transaction)).toList().isEmpty();
                return flag;
            }
        }
        return false;
    }
    public Branch findBranch(String nameOfBranch){
       return  branches.stream().filter(branch-> branch.getName().equals(nameOfBranch)).findFirst().orElse(null);
    }
    public boolean listCustomers(String nameOfBranch,boolean printTransactions){
        for (Branch branch:
           branches  ) {
            if(branch.getName().equals(nameOfBranch)){
                ArrayList<Customer>customers=branch.getCustomers();
                System.out.printf("Customer details for branch %s\n",branch.getName());
                System.out.println("List of customers:");
                for (Customer customer:
                     customers) {
                    System.out.printf("%s) %s\n",customers.indexOf(customer)+1,customer.getName());
                    if(printTransactions){
                        System.out.println("List of transactions:");
                        for (double transaction:
                            customer.getTransactions()) {
                            System.out.printf("%s. %s\n",customer.getTransactions().indexOf(transaction)+1,transaction);
                        }
                    }
                }


                return true;
            }
        }
        return false;
    }
}
