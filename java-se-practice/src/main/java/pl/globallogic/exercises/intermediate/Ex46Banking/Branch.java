package pl.globallogic.exercises.intermediate.Ex46Banking;

import java.util.ArrayList;

public class Branch  {
    protected Customer customer;

    protected String name;
    protected ArrayList<Customer> customers;

    public Branch(String name) {
        this.name = name;
        customers=new ArrayList<>();
    }
    public String getName() {
        return name;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }
    public boolean newCustomer(String nameOfCustomer,double initialTransaction){
        ArrayList<Double> transactionsOfNewCust=new ArrayList<>();
        transactionsOfNewCust.add(initialTransaction);
        Customer newCustomer=new Customer(nameOfCustomer,transactionsOfNewCust);
        customers.add(newCustomer);
        if(customers.contains(newCustomer)){
            return true;
        }
        else {
            return false;
        }
    }
    public boolean addCustomerTransaction(String nameOfCustomer,double transaction){
        for (Customer customer:
            customers ) {
            if(nameOfCustomer.equals(customer.getName())){
                ArrayList<Double> custTr=customer.getTransactions();
                custTr.add(transaction);
                return true;
            }

        }
        return false;
    }
    public Customer findCustomer(String nameOfC){
        for (Customer customer:
            customers ) {
            if(nameOfC.equals(customer.getName())){
                return customer;
            }
        }
        return null;
    }
}
