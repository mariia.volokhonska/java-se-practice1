package pl.globallogic.exercises.intermediate.Ex46Banking;

import java.util.ArrayList;

public class Customer {
    protected String name;
    protected ArrayList<Double> transactions;

    public Customer(String name, ArrayList<Double> transactions) {
        this.name = name;
        this.transactions = transactions;
    }

    public String getName() {
        return name;
    }

    public  ArrayList<Double> getTransactions() {
        return transactions;
    }
    public void addTransaction(double transaction){
        transactions.add(transaction);
    }
}
