package pl.globallogic.exercises.intermediate.Ex47AbstractClass;

public abstract class ListItem {
    protected Node rightLink;
    protected Node leftLink;



    protected Integer value;



    ListItem(Integer value){
        this.value=value;
    }
    public Integer getValue() {
        return value;
    }
    public void setValue(Integer value) {
        this.value = value;
    }
    abstract Node next();
    abstract Node setNext(Node right);
    abstract Node previous();
    abstract Node setPrevious(Node left);
    abstract int compareTo(Node item);


}
