package pl.globallogic.exercises.intermediate.Ex47AbstractClass;

public class Node extends ListItem{

    Node(Integer value){
        super(value);
    }
    @Override
    Node next(){
        return rightLink;
    }
    @Override
    Node setNext(Node right){
        rightLink=right;
        return rightLink;
    }
    @Override
    Node previous(){
        return leftLink;
    }
    @Override
    Node setPrevious(Node left){
        leftLink=left;
        return leftLink;
    }
    @Override
    int compareTo(Node item){
        Integer newValue=this.value;
        Integer previousValue=item.getValue();
        if(previousValue>newValue){
          return -1;
        }
        else if(previousValue<newValue){
           return 1;
        }
        else{
            return 0;
        }
    }

}
