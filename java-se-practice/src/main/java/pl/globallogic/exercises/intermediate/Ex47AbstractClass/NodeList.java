package pl.globallogic.exercises.intermediate.Ex47AbstractClass;

public interface NodeList {
    abstract Node getRoot();
    abstract boolean addItem(Node item);
    abstract boolean removeItem(Node item);
    abstract boolean traverse();

}
