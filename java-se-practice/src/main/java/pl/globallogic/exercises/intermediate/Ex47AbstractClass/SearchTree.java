package pl.globallogic.exercises.intermediate.Ex47AbstractClass;

public class SearchTree implements NodeList{
    Node root;

    public SearchTree(Node root) {
        this.root = root;
    }
    @Override
    public Node getRoot() {
        return root;
    }
    @Override
    public boolean addItem(Node item) {
        return true;
    }
    @Override
    public boolean removeItem(Node item){
        return true;
    }
    private void performRemoval(ListItem item, ListItem parentItem){
        System.out.printf("Item is %s, parent item is %s\n",item,parentItem);

    }
    @Override
    public boolean traverse(){
        if(root.equals(null)){
            System.out.println("List is empty");
        }
        return true;
    }
}
