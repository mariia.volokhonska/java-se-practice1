package pl.globallogic.exercises.intermediate.Ex48Interface;

import java.util.List;

public interface ISaveable {
    abstract List<String> read();
    abstract void write(List<String> list);
}
