package pl.globallogic.exercises.intermediate.Ex48Interface;

import java.util.ArrayList;
import java.util.List;

public class Monster implements ISaveable{
    String name;
    int hitPoints;
    int strength;
    List<String>listOfFields=new ArrayList<>();

    public Monster(String name, int hitPoints, int strength) {
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
    }

    public String getName() {
        return name;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getStrength() {
        return strength;
    }
    @Override
    public List<String> read(){
        if(listOfFields.isEmpty()||listOfFields.equals(null)){
            return null;
        }
        return listOfFields;
    }
    @Override
    public void write(List<String>list){
        listOfFields.add(name);
        listOfFields.add(Integer.toString(hitPoints));
        listOfFields.add(Integer.toString(strength));
    }
    @Override
    public String toString(){
        String str ="Moster{name='"+listOfFields.get(0)+"', hitPoints="+listOfFields.get(1)+", strength="+listOfFields.get(2)+"}";
        return str;
    }
}
