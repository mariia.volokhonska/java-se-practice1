package pl.globallogic.exercises.intermediate.Ex48Interface;

import java.util.ArrayList;
import java.util.List;

public class Player implements ISaveable {
    private String name;
    private String weapon;
    private int hitPoints;
    private int strength;
    private List<String>listOfFields=new ArrayList<>();

    public Player(String name, int hitPoints, int strength) {
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
        this.weapon="Sword";
    }

    public String getName() {
        return name;
    }

    public String getWeapon() {
        return weapon;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getStrength() {
        return strength;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }
    @Override
    public List<String> read(){
        if(listOfFields.isEmpty()||listOfFields.equals(null)){
            return null;
        }
        return listOfFields;
    }
    @Override
    public void write(List<String>list){
        listOfFields.add(name);
        listOfFields.add(Integer.toString(hitPoints));
        listOfFields.add(Integer.toString(strength));
        listOfFields.add(weapon);
    }
    @Override
    public String toString(){
        String str ="Player{name='"+listOfFields.get(0)+"', hitPoints="+listOfFields.get(1)+", strength="+listOfFields.get(2)+", weapon='"+listOfFields.get(3)+"'}";
        return str;
    }

}
