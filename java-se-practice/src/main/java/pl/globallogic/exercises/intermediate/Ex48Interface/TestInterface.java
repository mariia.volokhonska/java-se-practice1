package pl.globallogic.exercises.intermediate.Ex48Interface;

import java.util.ArrayList;
import java.util.List;

public class TestInterface {
    public static void main(String[] args) {
        Player player1=new Player("Max",24,50);

        List<String>playersData=new ArrayList<>();
        player1.write(playersData);
        player1.read();
        System.out.println(player1.toString());
        Monster monster1=new Monster("Vampire",30,60);

        List<String>vampiresData=new ArrayList<>();
        monster1.write(vampiresData);
        monster1.read();
        System.out.println(monster1.toString());


    }
}
