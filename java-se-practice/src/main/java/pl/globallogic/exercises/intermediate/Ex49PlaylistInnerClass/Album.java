package pl.globallogic.exercises.intermediate.Ex49PlaylistInnerClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class Album {
    private String name;
    private String artist;
    ArrayList<Song>songs1=new ArrayList<>();




    public void setSongs(ArrayList<Song> songs) {
        this.songs1=songs;
    }

    public Album(String name, String artist) {
        this.name = name;
        this.artist = artist;
    }
    public static class SongList{
        private ArrayList<Song> songs=new ArrayList<>();
        public SongList(ArrayList<Song>songs){
            this.songs=songs;
        }
        private boolean add(Song newSong){
            if(songs.contains(newSong)){
                return false;
            }
            else{
                songs.add(newSong);
                return true;
            }

        }
        private Song findSonginList(String titleOfSong){
            for (Song song:
                    songs  ) {
                if(titleOfSong.equals(song.getTitle())){
                    return song;
                }
            }
            return null;
        }
        private Song findSonginList(int trackNumber){
            if(trackNumber>songs.size()-1){
                return null;
            }
            else{
                return songs.get(trackNumber);
            }

        }

    }
    SongList songList =new SongList(songs1);
    public void addSong(String titleOfSong,double durationOfSong){
        Song newSong=new Song(titleOfSong,durationOfSong);
        songList.add(newSong);
    }
    public void findSong(String titleOfSong){
        songList.findSonginList(titleOfSong);
    }
    public void findSong(int numOfTrack){
        songList.findSonginList(numOfTrack);
    }


    public boolean addToPlayList(int trackNumberInAlbum, LinkedList<Song> playlist){
        if(trackNumberInAlbum<=songList.songs.size()){
            if(!playlist.contains(songList.songs.get(trackNumberInAlbum-1))){
                playlist.addFirst(songList.songs.get(trackNumberInAlbum-1));
                return true;}
        }

        return false;

    }
    public boolean addToPlayList(String titleOfSong, LinkedList<Song> playlist){
        for (Song song:
            songList.songs ) {
            if(titleOfSong.equals(song.getTitle())) {
                if(!playlist.contains(song)) {
                    playlist.addFirst(song);
                    return true;
                }

            }

        }
        return false;
    }
    public static void showPlayList(LinkedList<Song> playList){
        int i=1;
        for (Song song:
            playList ) {
            System.out.printf("%s.Song and Duration: %s\n",i,song.toString());
            i++;
        }
    }
}
