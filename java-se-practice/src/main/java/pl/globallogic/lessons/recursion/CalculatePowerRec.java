package pl.globallogic.lessons.recursion;

public class CalculatePowerRec {
    public static void main(String[] args) {
        int number=16;
        System.out.println(number==calculatePowerRec(2,4));
    }
    private static long calculatePowerRec(int base, int power) {
        if (power == 0) return 0;
        if (power == 1) return base;
        return base * calculatePowerRec(base, power - 1);
    }
}
