package pl.globallogic.lessons.recursion;

public class DigitalCounterRec {
    public static void main(String[] args) {
        int number=123;
        System.out.println(number==numberOfDigitsRec(number));
    }
    private static int numberOfDigitsRec(int targetNumber) {
        if ( targetNumber < 0 ) return -1;
        if ( targetNumber / 10 == 0) return 1;
        return numberOfDigitsRec(targetNumber / 10) + 1;
    }
}
