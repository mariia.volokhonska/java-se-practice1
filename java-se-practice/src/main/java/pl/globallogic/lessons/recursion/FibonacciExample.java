package pl.globallogic.lessons.recursion;

public class FibonacciExample {
    public static void main(String[] args) {
        int number=8;
        int result=13;
        System.out.printf("%s number in Fibonacci row is:%s\n",number,fibonacciRecursive(number));
        System.out.printf("%s number in Fibonacci row is:%s\n",number,fibonacciIterative(number));
    }
    private static int fibonacciRecursive(int number){
        if(number<2){
            return number;
        }
        else{
            return fibonacciRecursive(number-1)+fibonacciRecursive(number-2);
        }
    }
    private static int fibonacciIterative(int number){
        int last=0,next=1;
        for(int i=0;i<number;i++){
            int oldPrev=last;
            last=next;
            next=oldPrev+next;
        }
        return last;
    }
}
