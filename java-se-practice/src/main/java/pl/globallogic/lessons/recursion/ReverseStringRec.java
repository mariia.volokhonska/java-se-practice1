package pl.globallogic.lessons.recursion;


public class ReverseStringRec {
    public static void main(String[] args) {
        String string="abcds";
        String result="sdcba";
        System.out.println(result.equals(reverseRecursive(string)));
        System.out.println(result.equals(reverseInplace(string)));

    }
    private static String reverseRecursive(String source) {
        if (source.length() <= 1) return source;
        char firstCh = source.charAt(0);
        String remainder = source.substring(1);
        return reverseRecursive(remainder) + firstCh;
    }
    private static String reverseInplace( final String source) {
        final char[] sourceAsChars = source.toCharArray();
        int left = 0;
        int right = source.length() - 1;
        while (left < right) {
            final char leftChar = sourceAsChars[left];
            final char rightChar = sourceAsChars[right];
            sourceAsChars[left] = rightChar;
            sourceAsChars[right] = leftChar;
            left++;
            right--;
        }
        return String.valueOf(sourceAsChars);
    }

}
